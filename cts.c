#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <errno.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <zlib.h>

#define SIZE(X) (sizeof(X)/sizeof((X)[0]))

void readcell(void);

int accept(const char *s);
char* getstr(uint32_t *len);
uint32_t getuint32b(void);
uint32_t getuint32l(void);
unsigned char peek(void);
void* readall(int fd, int64_t *len);
void readstr(void);
void skippng(void);

void die(const char *fmt, ...);

static unsigned char *buf;
static int64_t blen, bpos;

int
main(int argc, char *argv[])
{
	int fd;
	
	if (argc > 1)
		fd = open(argv[1], O_RDONLY);
	else
		fd = STDIN_FILENO;
	
	if (fd < 0)
		die("failed to open input: %s:", argv[1]);
	buf = readall(fd, &blen);
	if (!buf)
		die("read error:");
	
	/* dump header */
	{
		int64_t pngat;
		if (!accept("TSFF"))
			die("missing file signature\n");
		if (peek() != 19)
			die("unsupported .cts format version %d, expected 19\n", peek());
		bpos++;
		while (!accept("D")) {
			if (!accept("I"))
				die("expected an Image element at $%06lX\n", bpos);
			bpos += 10;	/* skip scale value (80-bit float) */
			pngat = bpos;
			skippng();
			printf("PNG at $%06lX, size %ld\n", pngat, bpos-pngat);
		}
	}
	
	/* decompress the rest of the file */
	{
		z_stream zs;
		int status;
		int64_t unzcap = 3 * (blen - bpos), unzlen;
		unsigned char *unzbuf = malloc(unzcap);
		if (!unzbuf)
			die("out of memory\n");
		
		zs.zalloc = Z_NULL;
		zs.zfree = Z_NULL;
		zs.next_in = buf + bpos;
		zs.avail_in = blen - bpos;
		zs.next_out = unzbuf;
		zs.avail_out = unzcap;
		
		if (inflateInit(&zs))
			die("zlib error: %s\n", zs.msg);
		
		fprintf(stderr, "decompressing %ld bytes\n", blen - bpos);
		do {
			switch ((status = inflate(&zs, Z_FINISH))) {
			case Z_NEED_DICT:
				die("decompression error: zlib: missing dictionary\n");
			case Z_DATA_ERROR:
			case Z_STREAM_ERROR:
				die("decompression error: zlib: %s\n", zs.msg);
			case Z_MEM_ERROR:
				die("decompression error: zlib: out of memory\n");
			case Z_BUF_ERROR:
				unzcap *= 2;
				unzlen = zs.next_out - unzbuf;
				unzbuf = realloc(unzbuf, unzcap);
				if (!unzbuf)
					die("decompression error: out of memory\n");
				zs.next_out = unzbuf + unzlen;
				zs.avail_out = unzcap - unzlen;
			}
		} while (status != Z_STREAM_END);
		inflateEnd(&zs);
		free(buf);
		buf = unzbuf;
		blen = zs.next_out - buf;
		bpos = 0;
		fprintf(stderr, "decompressed %ld bytes\n", blen);
	}
	
	/* dump cells */
	readcell();
	
	/* read tags */
	{
		uint32_t len;
		char *s;
		do {
			s = getstr(&len);
			fwrite(s, 1, len, stdout);
			printf("\n");
		} while (len);
	}
	
	if (bpos < blen)
		fprintf(stderr, "%ld bytes of trailing junk at the end of file\n", blen - bpos);
	
	return 0;
}

void
readcell(void)
{
	const char *celltype[] = {"CT_DATA", "CT_CODE", "CT_VARD", "CT_VIEWH", "CT_VARU", "CT_VIEWV"};
	const char *drawstyle[] = {"grid", "blob", "line"};
	int hastext, hasnode;
	int i;
	uint32_t w, h;
	i = peek();
	if (i >= SIZE(celltype))
		die("unrecognized cell type %d at D+$%06lX\n", i, bpos);
	printf("type %s\n", celltype[i]);
	bpos++;
	
	getuint32l();
	getuint32l();
	
	i = peek();
	if (i >= SIZE(drawstyle))
		die("unrecognized cell draw style %d at D+$%06lX\n", i, bpos);
	printf("style %s\n", drawstyle[i]);
	bpos++;
	
	switch (peek()) {
	case 0: hastext = 1; hasnode = 0; break;
	case 1: hastext = 0; hasnode = 1; break;
	case 2: hastext = 1; hasnode = 1; break;
	case 3: hastext = 0; hasnode = 0; break;
	default:
		die("unknown cell content type %d\n", peek());
	}
	bpos++;
	
	if (hastext) {
		printf("text\n");
		readstr();
		printf("\nrel_size %d\n", getuint32l());
		if ((i = getuint32l()) < 0xffffffff)
			printf("image_i %d\n", i);
		printf("style_flags %d\n", getuint32l());
		bpos += 8;	/* TODO: last edit time */
	}
	
	if (hasnode) {
		printf("nodes\n");
		w = getuint32l();
		h = getuint32l();
		printf("grid %dx%d\n", w, h);
		getuint32l();
		getuint32l();
		bpos += 2;
		for (i = 0; i < w; i++)
			getuint32l();
		for (i = 0; i < w*h; i++)
			readcell();
	}
}

int
accept(const char *s)
{
	int slen = strlen(s);
	if (bpos+slen >= blen)
		die("input ends unexpectedly\n");
	if (memcmp(buf+bpos, s, slen) == 0) {
		bpos += slen;
		return 1;
	}
	return 0;
}

char*
getstr(uint32_t *len)
{
	uint32_t sl = getuint32l();
	char *s = (char*) buf + bpos;
	if (len)
		*len = sl;
	bpos += sl;
	return s;
}

uint32_t
getuint32b(void)
{
	uint32_t i;
	if (bpos + 4 >= blen)
		die("input ends unexpectedly\n");
	i = buf[bpos] << 24;
	i |= buf[bpos+1] << 16;
	i |= buf[bpos+2] << 8;
	i |= buf[bpos+3];
	bpos += 4;
	return i;
}

uint32_t
getuint32l(void)
{
	uint32_t i;
	if (bpos + 4 > blen)
		die("input ends unexpectedly\n");
	i = buf[bpos];
	i |= buf[bpos+1] << 8;
	i |= buf[bpos+2] << 16;
	i |= buf[bpos+3] << 24;
	bpos += 4;
	return i;
}

unsigned char
peek(void)
{
	if (bpos >= blen)
		die("input ends unexpectedly\n");
	return buf[bpos];
}

void*
readall(int fd, int64_t *len)
{
	int64_t bcap = 0, bs = 0;
	size_t n;
	char *buf = NULL;
	for (;;) {
		if (bs == bcap) {
			bcap += 64 * 1024;
			buf = realloc(buf, bcap);
		}
		if (!buf)
			return NULL;
		n = read(fd, buf + bs, bcap - bs);
		switch (n) {
		case -1:
			free(buf);
			return NULL;
		case 0:
			*len = bs;
			return buf;
		default:
			bs += n;
		}
	}
}

void
readstr(void)
{
	uint32_t len = getuint32l();
	printf("%d:", len);
	if (bpos + len >= blen)
		die("truncated string starting at D+$%06lX\n", bpos);
	fwrite(buf + bpos, 1, len, stdout);
	bpos += len;
}

void
skippng(void)
{
	int64_t clen;
	if (!accept("\x89PNG\r\n\x1a\n"))
		die("invalid PNG header at $%06lX\n", bpos);
	
	for (;;) {
		clen = getuint32b();
		if (accept("IEND")) {
			bpos += clen + 4;
			return;
		}
		
		bpos += clen + 8;
	}
}

void
die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}

	abort();
	exit(1);
}
